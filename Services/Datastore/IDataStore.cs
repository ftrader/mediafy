﻿using System.Collections.Generic;
using Mediafy.Models;

namespace Mediafy.Services.Datastore
{
    public interface IDataStore
    {
        IEnumerable<ProductEntity> GetProducts();
        void AddProducts(IEnumerable<ProductModel> newProducts);
        void Save();
    }
}