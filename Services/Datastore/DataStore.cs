﻿using System.Collections.Generic;
using Mediafy.Models;

namespace Mediafy.Services.Datastore
{
    public class DataStore : IDataStore
    {
        private List<ProductEntity> Products { get; set; }

        public DataStore()
        {
            Products = new List<ProductEntity>();
        }

        public IEnumerable<ProductEntity> GetProducts()
        {
            return Products;
        }

        public void AddProducts(IEnumerable<ProductModel> newProducts)
        {
            foreach (var newProduct in newProducts)
            {
                Products.Add(new ProductEntity { Product = newProduct, ViewCount = 0 });
            }
        }

        public void Save()
        {
            // Don't forget to fake save :)
        }
    }
}