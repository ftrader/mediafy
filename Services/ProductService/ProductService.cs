﻿using System.Collections.Generic;
using System.IO;
using Mediafy.Models;
using Newtonsoft.Json;
using Slugify;

namespace Mediafy.Services.ProductService
{
    public interface IProductService
    {
        List<ProductModel> GetProducts();
    }
    
    public class ProductService : IProductService
    {
        public List<ProductModel> GetProducts()
        {
            using (var r = new StreamReader(Path.Combine(Directory.GetCurrentDirectory(), "data\\products.json")))
            {
                var jsonFile = r.ReadToEnd();
                var products = JsonConvert.DeserializeObject<List<ProductModel>>(jsonFile);
                
                // Add slug so urls look a bit better
                var slugHelper = new SlugHelper();
                foreach (var productModel in products)
                {
                    productModel.Slug = slugHelper.GenerateSlug(productModel.Name);
                }

                return products;
            }
        }
    }
}
