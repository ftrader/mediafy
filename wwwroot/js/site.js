﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

$(function() {
    
    $('.js-productcard').on('click', (e) => {
        window.location.href = $(e.currentTarget).data('url');
    });
    
});