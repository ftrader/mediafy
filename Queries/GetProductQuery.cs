﻿using System.Collections.Generic;
using System.Linq;
using Mediafy.Models;
using Mediafy.Services.Datastore;
using Mediafy.Services.ProductService;

namespace Mediafy.Queries
{
    public class GetProductQuery : IGetProductQuery
    {
        private readonly IProductService _productService;
        private readonly IDataStore _dataStore;

        public GetProductQuery(IProductService productService, IDataStore dataStore)
        {
            _productService = productService;
            _dataStore = dataStore;
        }

        public List<ProductModel> GetProducts()
        {
            if (_dataStore.GetProducts().Any())
            {
                return _dataStore.GetProducts()
                    .OrderByDescending(x => x.ViewCount)
                    .Select(x => x.Product)
                    .ToList();
            }
            
            var products = _productService.GetProducts();
            _dataStore.AddProducts(products);
            _dataStore.Save();
            return products;
        }

        public ProductModel GetProduct(string slug)
        {
            var entity = _dataStore.GetProducts().First(x => x.Product.Slug == slug);
            entity.ViewCount++;
            _dataStore.Save();
            return entity.Product;
        }
    }
}