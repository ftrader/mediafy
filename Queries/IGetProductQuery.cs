﻿using System.Collections.Generic;
using Mediafy.Models;

namespace Mediafy.Queries
{
    public interface IGetProductQuery
    {
        ProductModel GetProduct(string slug);
        List<ProductModel> GetProducts();
    }
}
