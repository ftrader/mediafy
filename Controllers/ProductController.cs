﻿using Mediafy.Models;
using Mediafy.Queries;
using Microsoft.AspNetCore.Mvc;

namespace Mediafy.Controllers
{
    public class ProductController : Controller
    {
        private readonly IGetProductQuery _getProductQuery;

        public ProductController(IGetProductQuery getProductQuery)
        {
            _getProductQuery = getProductQuery;
        }

        public ActionResult<ProductViewModel> Index()
        {
            var products = _getProductQuery.GetProducts();
            return View(new ProductViewModel(products));
        }
        
        [HttpGet("/{productName}")]
        public ActionResult<ProductModel> ProductCard(string productName)
        {
            var product = _getProductQuery.GetProduct(productName);
            return View(product);
        }
    }
}
