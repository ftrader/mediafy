﻿using Newtonsoft.Json;

namespace Mediafy.Models
{
    public class ProductModel
    {
        [JsonProperty("name")]
        public string Name { get; set; }
        
        [JsonProperty("fromPrice")]
        public string FromPrice { get; set; }
        
        [JsonProperty("image")]
        public string Image { get; set; }
        
        [JsonProperty("description")]
        public string Description { get; set; }
        
        public string Slug { get; set; }
    }
}