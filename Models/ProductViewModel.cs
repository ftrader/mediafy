﻿using System.Collections.Generic;

namespace Mediafy.Models
{
    public class ProductViewModel
    {
        public ProductViewModel(List<ProductModel> products)
        {
            Products = products;
        }

        public List<ProductModel> Products { get; set; }
    }
}