﻿namespace Mediafy.Models
{
    public class ProductEntity
    {
        public ProductModel Product { get; set; }
        public int ViewCount { get; set; }
    }
}
